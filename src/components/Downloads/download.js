import Resume from "../../Assets/Pdfs/Aditya_Gandhi_Resume.pdf";
import React from "react";
import Wrapper from "../Wrapper.js";
import Particle from "../Particle.tsx";
import { Container, Row, Col, Button } from "react-bootstrap";
import { AiOutlineArrowDown } from "react-icons/ai";
import { Document, Page, pdfjs } from 'react-pdf/dist/esm/entry.webpack';
import "./download.css";
// import Wrapper from "../Wrapper.js";
pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

// import { Card, CardMedia } from "@mui/material";
export default function Download(){
    const Resume2 = require("../../Assets/Pdfs/Aditya_Gandhi_Resume.pdf");

    const pdfUrl = "../../Assets/Pdfs/Aditya_Gandhi_Resume.pdf";
    const onButtonClick = () => {
        const pdfUrl2 = Resume2;
        const link = document.createElement("a");
        link.href = pdfUrl2;
        link.download = 'Aditya_Gandhi_Resume.pdf'; // specify the filename
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    };
    return(
        <Wrapper>
        <Container fluid className="download-container">
            {/* <Particle /> */}
            {/* <Wrapper> */}
            <Row>
                <Col xs={12}>
                    <Button size="lg" variant="primary" className="download-btn" style={{zIndex: '9999'}} onClick={onButtonClick}><AiOutlineArrowDown/> Download</Button>
                </Col>
            </Row>
            <Row xs={12}>
                <Col xs={12} style={{justifyContent: 'center', alignItems: 'center', display: 'flex', marginTop: '2em'}}>

                        <Document file={Resume} style={{ }} id='downdload-resume'>
                            <Page pageNumber={1} />
                        </Document>
               
                </Col>
            </Row>
            {/* <Row className="resume">
                <Card sx={{ maxWidth: 824 }}>
                    <CardMedia
                        className="cardmedia"
                        component="iframe"
                        Height="1056px"
                        src="https://drive.google.com/file/d/13YNqvTbDnhzBVVPY6IUlvGA_ZsZZuxPu/preview"
                    />
                </Card>
            </Row> */}
            
            {/* </Wrapper> */}
            
        </Container>
        </Wrapper>
    );
}